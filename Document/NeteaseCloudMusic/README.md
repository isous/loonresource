#### 使用说明

1. 在需要使用共享账号的iOS设备上安装[网易云音乐共享会员](https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/NeteaseCloudMusic_shared_membership.plugin)插件；
2. 建议用临时禁用`MitM over HTTP/2`再抓包，否则HTTP/2下会有很多个`Cookie`存在，导致无法区分，抓完之后重新启用`MitM over HTTP/2`；
3. 从其他设备上抓包，并将得到的`Cookie`、`User-Agent`和`MConfig-Info`填写进`网易云音乐共享会员`插件的配置页面里对应的`163_Cookie`、`163_User-Agent`和`163_MConfig-Info`中；
4. 至此共享会员配置完毕。

- 需要注意的是，提供大会员Cookie的设备一旦将账号退出，则此Cookie失效，需要重新按照上面的步骤抓取填写；
- 如果出现问题，可以尝试清除`163_Cookie`、`163_User-Agent`和`163_MConfig-Info`的持久化缓存。