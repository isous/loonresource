#### 使用说明

##### 我拥有iOS备用机

1. 你需要iOS备用机上安装[Bilibili Cookie捕获](https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Bilibili_cookie_extraction.plugin)插件；
2. 在需要使用共享账号的另一台iOS设备上安装[Bilibili共享会员](https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Bilibili_shared_membership.plugin)插件；
3. 在确保`Bilibili Cookie捕获`插件已经启用，且`大会员`账号已经登录iOS备用机的前提下，重新打开Bilibili应用会弹出`Bilibili Cookie获取成功`的通知；
4. 在`仪表`页面进入`脚本任务`，找到名为`Bilibili Cookie捕获`的任务，并并点击进入；
5. 查看此脚本运行日志，并将日志中对应字段的内容发送给需要使用共享账号的iOS设备上，并填写至`Bilibili共享会员`插件的配置页面`Bilibili_buvid`、`Bilibili_Authorization`和`Bilibili_User-Agent`中；
6. 完成第五步之后，需要禁用`Bilibili Cookie捕获`插件，避免频繁捕获Cookie；
7. 至此共享会员配置完毕。

##### 我没有iOS备用机

1. 你需要凭本事获得大会员账号的`Cookie`；
2. 在需要使用共享账号的iOS设备上安装[Bilibili共享会员](https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Bilibili_shared_membership.plugin)插件，如果你会看抓包内容的话也可以不装；
3. 将获取到的`buvid`、`Authorization`和`User-Agent`填写进`Bilibili共享会员`插件的配置页面的`Bilibili_buvid`、`Bilibili_Authorization`和`Bilibili_User-Agent`中；
4. 至此共享会员配置完毕。

- 需要注意的是，提供大会员Cookie的设备一旦将账号退出，则此Cookie失效，需要重新按照上面的步骤抓取填写；
- 如果出现问题，可以尝试清除`Bilibili_buvid`、`Bilibili_Authorization`和`Bilibili_User-Agent`的持久化缓存；
- 抓取Cookie也并非一定要`Bilibili Cookie捕获`插件才可以，任何能抓包的工具，都可以抓取到Cookie。