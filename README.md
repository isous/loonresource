<table>
  <tr>
    <th>类别</th>
    <th>序号</th>
    <th>名称</th>
  </tr>
  <tr>
    <td rowspan="42"><strong>解锁插件</strong></td>
  <tr>
    <td>01</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/A_Translator_crack.plugin">A Translator解锁</a></td>
  </tr>
  <tr>
    <td>02</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Adobe_Creative_Cloud_crack.plugin">Adobe Creative Cloud解锁</a></td>
  </tr>
  <tr>
    <td>03</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Agenda_crack.plugin">Agenda解锁</a></td>
  </tr>
  <tr>
    <td>04</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Aiinquiry_crack.plugin">爱企查解锁</a></td>
  </tr>
  <tr>
    <td>05</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/AnyDown_crack.plugin">AnyDown解锁</a></td>
  </tr>
  <tr>
    <td>06</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/AppRaven_crack.plugin">AppRaven解锁</a></td>
  </tr>
  <tr>
    <td>07</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Audiomack_crack.plugin">Audiomack解锁</a></td>
  </tr>
  <tr>
    <td>08</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/BaiduNetDisk_crack.plugin">百度网盘解锁</a></td>
  </tr>
  <tr>
    <td>09</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/BaiduWenku_crack.plugin">百度文库解锁</a></td>
  </tr>
  <tr>
    <td>10</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Bilibili_cookie_extraction.plugin">Bilibili Cookie捕获</a></td>
  </tr>
  <tr>
    <td>11</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Cubox_crack.plugin">Cubox解锁</a></td>
  </tr>
  <tr>
    <td>12</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Darkroom_crack.plugin">Darkroom解锁</a></td>
  </tr>
  <tr>
    <td>13</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Documents_crack.plugin">Documents解锁</a></td>
  </tr>
  <tr>
    <td>14</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Drafts_crack.plugin">Drafts解锁</a></td>
  </tr>
  <tr>
    <td>15</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/DXY_Drugspro_crack.plugin">用药助手解锁</a></td>
  </tr>
  <tr>
    <td>16</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Emby_crack.plugin">Emby解锁</a></td>
  </tr>
  <tr>
    <td>17</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Enpass_Password_Manager_crack.plugin">Enpass Password Manager解锁</a></td>
  </tr>
  <tr>
    <td>18</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Foodie_crack.plugin">Foodie解锁</a></td>
  </tr>
  <tr>
    <td>19</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/GaoDing_crack.plugin">稿定设计解锁</a></td>
  </tr>
  <tr>
    <td>20</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Grow_crack.plugin">Grow解锁</a></td>
  </tr>
  <tr>
    <td>21</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/HappyDays_crack.plugin">HappyDays解锁</a></td>
  </tr>
  <tr>
    <td>22</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/HashPhotos_crack.plugin">HashPhotos解锁</a></td>
  </tr>
  <tr>
    <td>23</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/HelixLife_crack.plugin">解螺旋解锁</a></td>
  </tr>
  <tr>
    <td>24</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/imageX_crack.plugin">imageX解锁</a></td>
  </tr>
  <tr>
    <td>25</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/LanJiYin_crack.plugin">蓝基因解锁</a></td>
  </tr>
  <tr>
    <td>26</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/MIX_crack.plugin">MIX解锁</a></td>
  </tr>
  <tr>
    <td>27</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Nicegram_crack.plugin">Nicegram解锁</a></td>
  </tr>
  <tr>
    <td>28</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Notability_crack.plugin">Notability解锁</a></td>
  </tr>
  <tr>
    <td>29</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/OneAI_crack.plugin">OneAI解锁</a></td>
  </tr>
  <tr>
    <td>30</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/OneClickCutout_crack.plugin">一键抠图解锁</a></td>
  </tr>
  <tr>
    <td>31</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/ProCamera_crack.plugin">ProCamera解锁</a></td>
  </tr>
  <tr>
    <td>32</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/RevenueCat_crack.plugin">RevenueCat解锁</a></td>
  </tr>
  <tr>
    <td>33</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Scanner_Pro_crack.plugin">Scanner Pro解锁</a></td>
  </tr>
  <tr>
    <td>34</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/SearchPicture_crack.plugin">搜图神器解锁</a></td>
  </tr>
  <tr>
    <td>35</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Shuiyinbao_crack.plugin">水印宝解锁</a></td>
  </tr>
  <tr>
    <td>36</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Slidebox_crack.plugin">Slidebox解锁</a></td>
  </tr>
  <tr>
    <td>37</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Squirrel_Download_crack.plugin">松鼠下载解锁</a></td>
  </tr>
  <tr>
    <td>38</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/xCurrency_crack.plugin">极简汇率解锁</a></td>
  </tr>
  <tr>
    <td>39</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/XiaoQi_VideoGet_crack.plugin">视频下载解锁</a></td>
  </tr>
  <tr>
    <td>40</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Xmind_crack.plugin">Xmind解锁</a></td>
  </tr>
  <tr>
    <td>41</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/AnyVideoBox_crack.plugin">视频盒子解锁</a></td>
  </tr>
    <td></td>
    <td></td>
    <td></td>
  <tr>
    <th>类别</th>
    <th>序号</th>
    <th>名称</th>
  </tr>
  <tr>
    <td rowspan="5"><strong>会员共享模板插件</strong></td>
  <tr>
    <td>01</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/Bilibili_shared_membership.plugin">Bilibili共享会员</a></td>
  </tr>
  <tr>
    <td>02</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/CaixinMedia_shared_membership.plugin">财新共享会员</a></td>
  </tr>
  <tr>
    <td>03</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/ColorfulClouds_shared_membership.plugin">彩云天气Pro共享会员</a></td>
  </tr>
  <tr>
    <td>04</td>
    <td><a href="https://www.nsloon.com/openloon/import?plugin=https://gitlab.com/isous/loonresource/-/raw/main/Plugin/NeteaseCloudMusic_shared_membership.plugin">网易云音乐共享会员</a></td>
  </tr>
</table>