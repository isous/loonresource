// 2024-07-13 12:15

if (!$response) $done({});
if (!$response.body) $done({});
var url = $request.url;
var header = $request.headers;
const isQuanX = typeof $task !== "undefined";

// 到期时间 即将到期
const myAppinfo = "";
const myUid = "uid=" + $persistentStore.read("cx_uid");
const myCode = "code=" + $persistentStore.read("cx_code");
const myDevice = "device=" + $persistentStore.read("cx_device");

if (url.includes("/validateAudioAuth") || url.includes("/groupImageValidate")) {
  // 会员数据
  header["appinfo"] = myAppinfo;
  delete header["authentication"];
  if (isQuanX) {
    delete header["Cookie"];
    delete header["User-Agent"];
    delete header["requestTime"];
  } else {
    delete header["cookie"];
    delete header["requesttime"];
    delete header["user-agent"];
  }
  $done({ headers: header });
} else if (url.includes("/validate?") || url.includes("/neWValidate?")) {
  // 会员数据
  url = url
    .replace(/uid=\d+/g, myUid)
    .replace(/code=\w+/g, myCode)
    .replace(/device=\w+/g, myDevice)
    .replace(/deviceType=\d+/g, "deviceType=1")
    .replace(/&_t=\d+/g, "");
  if (isQuanX) {
    delete header["Cookie"];
    delete header["Referer"];
    delete header["User-Agent"];
  } else {
    delete header["cookie"];
    delete header["referer"];
    delete header["user-agent"];
  }
  $done({ url: url, headers: header });
}