let obj = JSON.parse($response.body);
const url = $request.url;

const sy = '/editors/risk_materials';
const my = '/structure/company';
const ad = '/template/resources';

if (url.includes(sy)) {
    obj.has_purchased = false;
    obj.risk_materials = [];
    obj.risk_fonts = [];
    obj.product_materials = [];
    obj.user_right = null;
    obj.risk_fonts = [];
} else if (url.includes(my)) {
    let newObj = JSON.parse($response.body);
    newObj.modules[0].product_info.product_name = "团队版·终身";
    newObj.modules[0].product_info.is_expired = true;
    newObj.modules[0].product_info.lifetime_vip = true;
    newObj.modules[0].product_info.product_type = "TEAM_LIFETIME_VIP";
    newObj.product = {
        product_name: "团队版·终身",
        is_expired: true,
        lifetime_vip: true,
        product_id: "300", 
        product_type: "TEAM_LIFETIME_VIP"
    };

    $done({ body: JSON.stringify(newObj) });
} else if (url.includes(ad)) {
    var json = JSON.parse($response.body);
    var banner = json.pits[0].delivery_materials;
    banner.forEach(function (bannerData) {
        for (var prop in bannerData) {
            bannerData[prop] = "";
        }
    });
    $done({ body: JSON.stringify(json) });
}

$done({ body: JSON.stringify(obj) });
